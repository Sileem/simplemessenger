/**
 * 
 * Sileem Farghaly
 * CSE 486 PA1
 * Simple Messenger Client:
 * 		Create an android application that can send and receive 
 * 		messages between each other
 * edu.buffalo.cse.cse486586.simplemessenger
 * DUE: 2/1/2013
 * 
 */
package edu.buffalo.cse.cse486586.simplemessenger;

import java.net.ServerSocket;
import java.net.Socket;
import java.net.UnknownHostException;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.telephony.*;
import android.content.*;

public class MainActivity extends Activity {
	private EditText Sendfld; // Set to sendFld
	private String toSend; // Variable used to store the data from SendFld
	private boolean sendFlag = false;

	private TextView RecFld; // Set to fldRecieveText
	private String portStr; // Port string from notes

	private class ServerTask extends AsyncTask<Void, String, Void> {
		/**
		 * 
		 * This thread listens on port 10000 for any incoming messages From
		 * there it recieves the message and should display it.
		 * 
		 */
		@Override
		protected Void doInBackground(Void... arg0) {
			DataInputStream dataInputStream = null;
			ServerSocket server = null;
			Socket clientSocket = null;
			String recievedMessage = null;

			try {
				server = new ServerSocket(10000);
			} catch (IOException e) {
				e.printStackTrace();
			}
			while (true) {
				try {
					clientSocket = server.accept();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				try {
					dataInputStream = new DataInputStream(
							clientSocket.getInputStream());

					recievedMessage = dataInputStream.readUTF();
										
					publishProgress(recievedMessage);
					// RecFld.setText(reMSG);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}

		protected void onProgressUpdate(String... strings) {
			RecFld.setText(strings[0]);
		}
	}

	Thread ClientThread = new Thread() {
		/**
		 * 
		 * This thread handles sending the message. It can send to either port
		 * 11108 or 11112 depending on which AVD is active.
		 * 
		 */
		public void run() {
			Socket socket = null;
			DataOutputStream dataOutputStream = null;
			try {

				while (true) {
					if (sendFlag == true) {
						/**
						 * Determine which AVD is active, then select the port
						 * This has to be done each iteration.
						 **/
						if (portStr.equals("5556")) {
							socket = new Socket("10.0.2.2", 11108);
						} else {
							socket = new Socket("10.0.2.2", 11112);
						}
						
						sendFlag = false; // Reset the flag
												
						// Open Data Stream
						dataOutputStream = new DataOutputStream(
								socket.getOutputStream());
						dataOutputStream.writeUTF(toSend);
						
						socket.close();
					}
				}

			} catch (UnknownHostException e) {
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// Get the port string
		TelephonyManager tel = (TelephonyManager) this
				.getSystemService(Context.TELEPHONY_SERVICE);
		portStr = tel.getLine1Number().substring(
				tel.getLine1Number().length() - 4); // Copied from notes PA1

		// Set up the UI fields
		RecFld = (TextView) findViewById(R.id.fldRecieveText);
		Sendfld = (EditText) findViewById(R.id.sendFld);
		Button btnSend = (Button) findViewById(R.id.btnSend);

		// Start the listening to port 10000 after the UI is setup.
		new ServerTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
		// Initialize the sending task.
		ClientThread.start();

		// Monitor the Send button
		btnSend.setOnClickListener(ButtonSendOnClickListener);

	}

	Button.OnClickListener ButtonSendOnClickListener = new Button.OnClickListener() {
		@Override
		public void onClick(View v) {
			toSend = Sendfld.getText().toString();
			sendFlag = true;
		}
	};
}